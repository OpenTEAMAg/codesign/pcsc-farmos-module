## Documentation template

This MkDocs template was forked from the OpenTEAM Documentation Template site. 

## HTML webpage information

- site_name: PCSC farmOS Module
- site_url: https://openteamag.gitlab.io/codesign/pcsc-farmos-module

## About
This is a collaborative project between [OpenTEAM](https://openteam.community/), [farmOS](https://farmos.org/), and partners in support of our USDA Partnership for Climate-Smart Commodities project, [Action for Climate-Smart Agriculture](https://www.wolfesneck.org/partnerships-for-climate-smart-commodities/). 

This work is supported by a subaward from a federal grant, led by [Wolfe’s Neck Center for Agriculture and the Environment](https://www.wolfesneck.org/).

This material is based upon work supported by the U.S. Department of Agriculture, under agreement number NR233A750004G032.

Relevant data and stories will be shared with other USDA project awardees and USDA leadership to inform the next generation of grant funding and other climate-smart infrastructure supports. Data will only be shared by the USDA in aggregate and de-identified. Learn more about USDA’s Partnerships for Climate-Smart Commodities [here](https://www.usda.gov/climate-solutions/climate-smart-commodities).

## Learn more about mkdocs generally

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.


 We use a MkDocs template with a ReadTheDocs theme. For more information for how to format and contribute using this theme and structure, check out [mkdocs.readthedocs.io](https://mkdocs.readthedocs.io/en/restructure-compat/)

