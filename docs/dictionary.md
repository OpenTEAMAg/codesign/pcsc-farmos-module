# Dictionary of Data Fields - Registry

Both the FAIR Tech Registry Collabathon and the Conservation Benefits Catalog produced tentative categories for assessing tools or models, based on community fact-finding. The [Parameter Comparisons table here](https://docs.google.com/spreadsheets/d/12n2J38W3xFIezXSS0wtx5obUhYah8MVi_Lh2vH8EclY/edit#gid=1580521510) relates the categories or criteria identified by both the FAIR Tech Registry working group and the Meridian Conservation Benefits Catalog project as important for attestation.

The following tables show each data field, a description or definition, and the source of the initial field-- either the FAIR Tech Registry Collabathon (FTR), the Conservation Benefits Catalog (CBC), or both. 

## Minimal Categories / Criteria 

| Category             | Description                          | Source     | 
| -----------          | -----------                          | -----------| 
| Name                 | Tool or module name                  |      Both      |
| Short name           | Abbreviation, Including creating one if it doesn’t have one |  CBC          | 
| Agency or Organization    | Who most recently owns and/ or maintains the tool or asset  |         Both   | 
| Asset Type           | Categories include: Dataset, Software model, Standard, Reference, Program, Protocol, Procedure.  Based on what NRCS would call it.                                 |     CBC       |
| Model or software class      | Categories include: Conservation planning, decision support, data access, data collection, data storage, data visualization, expert synthesis, financial/logistics, geospatial analysis, literature aggregation, meta-modeling output, mobile app, model wrapper, primary, species selection.                         |    CBC        |
| Link to asset, source site, documentation  | Widely variable -- Sometimes this is a published paper, sometimes  a user manual.                                |     Both       |
| Text description      | Based on information provided by the team or publisher.   |      Both      | 
| Point of contact, contact email  | While this information is typically listed, it can be unclear if that person is still active or in touch. This could be a useful space for additional info, i.e. 'is this active?'                     |   Both         |
| Date of last update, version number  | Anecdotal note - if it lists a date or version, likely to be more recent/active                |    Both        |
| Metadata             | Very few tools here had machine-readable metadata, but good to link it when it was available.                                 |   Both         |
| Available to the public?    | Ontology list includes: Yes, No, Not yet (beta), Unknown, No longer, Upon request, Partially       |      CBC      |
| Has API?   | Yes/No (sometimes yes but secret) |      Both - in FTR as 'nice to have' info     |
| Built on/built with      | What is the tech stack? What programming language is the tool/module written in? What specific standards or existing ontologies are used?       |   FTR         |
| System requirements      | OS, versions, prerequisite software/environment, memory, storage |  FTR          |
| License   | License & terms of use        | FTR           |
| Statement on data storage, data privacy management      | Requirement for CARE - Mechanism for ‘Disclosure, consent, and control required with respect to secondary uses of research materials and data’ [(link)](https://static1.squarespace.com/static/5d3799de845604000199cd24/t/637acd803a7dab6b8a27dfd3/1668992386691/fgene-13-823309.pdf) |FTR | 
| Context for use, use documentation   | Any avilable information on who this was built for and why, including tutorials or support resources        | FTR           |
|



## Maximal / Project-Specific Categories 

Information coming soon -- [refer to this document in interim](https://docs.google.com/document/d/1YyP6ppfDu3CIBcKR0Nw_STgNd7EMMtwiJu0kFXLWkxQ/edit). 


