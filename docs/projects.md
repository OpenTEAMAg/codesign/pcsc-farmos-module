# Related Projects



## Background
Early in our process of developing data collection tools & structures for our PCSC project, OpenTEAM held an event with seven PCSC projects to map & compare our experiences. You can read about that work here: 

* [PCSC Event Storming Documentation](https://openteamag.gitlab.io/codesign/pcsc/)


We intend to keep working in collaboration across these and other projects to develop tools and components that can be reused for mutual benefit. 

### Common Farm Conventions

farmOS is actively engaged in conversations around building & providing a testing space for a set of common conventions around farm data. From their website:  

> One of the longer term goals of farmOS is to be a platform that supports the collaborative development of these conventions over time. As new standards are developed and adopted in the community, they can be written into modules that provide different levels of "enforcement of" or "compliance to" these conventions.

* [farmOS Data Model](https://farmos.org/model/)
* [farmOS Conventions](https://farmos.org/model/convention/)

We also host a biweekly community call to discuss these topics - you can find more information here: 

* **[farmOS forum - Conventions Community Call](https://farmos.discourse.group/t/common-farm-convention-biweekly-community-call/1914)**

