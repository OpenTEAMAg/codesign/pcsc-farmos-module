# Participating Orgs

## farmOS

[farmOS](https://farmos.org/) is a web-based application for farm management, planning, and record keeping. It is developed by a community of farmers, developers, researchers, and organizations with the aim of providing a standard platform for agricultural data collection and management.

This module is built on top of farmOS, using the farmOS data conventions for land assets, but  farmOS has greater data capabilities for granular, event-based record-keeping as needed. This is important for us as we are currently working with interlinked farmOS and PCSC data that could potentially be separated out in the future.   


## OpenTEAM

[OpenTEAM](https://openteam.community/), , or the Open Technology Ecosystem for Agricultural Management, is equipping food systems leaders with shared knowledge, collaborative frameworks for problem-solving, and open-source, connected technologies to build climate change resilience and thriving communities.

Through this work, we are collectively building an open source, interoperable tech ecosystem that enables farmer-control of data, knowledge sharing, and access to programs and marketplace incentives. 


## Wolfe's Neck Center - Action for Climate-Smart Practices 

[Wolfe's Neck Center for Agriculture and the Environment](https://www.wolfesneck.org/) is a nonprofit organization focused on creating a world where agriculture and food systems support farmer viability, thriving ecosystems, and vibrant communities. They are OpenTEAM's host organization. 

Through their [USDA Partnership for Climate-Smart Commodities program](https://www.usda.gov/climate-solutions/climate-smart-commodities), [Action for Climate-Smart Agriculture](https://www.wolfesneck.org/partnerships-for-climate-smart-commodities/), Wolfe’s Neck Center will collaboratively lead an investment of $35 million in advancing climate-smart agriculture.

## Other Collaborators 

Coming soon!

