# Contact Us

If you're an individual or organization interested in this project, please reach out to us through email  (vic@openteam.community), or make a post on [the farmOS forum](https://farmos.discourse.group/). 

You can also see & contribute issues & requests at the Github page for this module, [farm_pcsc](https://github.com/mstenta/farm_pcsc/issues). 

Thank you for your interest, and we look forward to hearing from you. 
