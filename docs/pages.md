# Using the Module

## Overview
### Dashboard

![dashboard](https://gitlab.com/OpenTEAMAg/codesign/pcsc-farmos-module/-/raw/ac936ef3357e91172e5b4cd2b5e65373f6e85dec/docs/assets/dashboard.png)

- **Map!** A simple map of all Land Asset geometries associated with enrolled Fields.
- **Quick Form links:** Can be used to access frequently used forms for creating new enrollment and summary records.
- **Reports:**
- - The current number of Producers enrolled. Links to Producers report.
- - The current number of Fields enrolled. Links to the Land Asset page.

### Menu

- **Locations**: A map of all Locations and Land Assets associated with enrolled Fields.
- **Records**: Links to pages that display tables of all FarmOS Core Asset and Log types.
You can see FarmOS Documentation on these but they are not required to understand or use the PCSC DPR data collection.
- **People**: All users in the PCSC system.
- **Reports**: Reports can be used as the entry point to find PCSC Producers and their relevant records. All report tables have filters that can be used to search for specific records.
> - **PCSC Producers**: Displays all enrolled Producers, Farm ID and State / County with links to the Producer’s page. Use the “checkboxes” on the left + Export PCSC CSV action to generate CSV files for the DPR.
> - **PCSC Farm Summaries**: Displays a table of Farm Summary records for all Producers.
> - **PCSC Field Summaries**: Displays a table of Field Summary records for all Producers. 


### Entering Data

Overall the process and user interface is similar for each type of record, and it all starts with a Producer. See the record types below for more detailed information, but an overview:

1) Use a form to create the initial record and associate it with a given Producer and/or Field Enrollment.
2) After these are created they will be visible and searchable in various "Tables" with links to View + Edit each record.
3) These tables will be visible in different parts of the system depending on the type of record. When in doubt, go to a Producer's page or an individual Field Enrollment page to find what you are looking for.

## Records

### Producer Enrollment

- Represents Producers and general information about the farm. Producers must be enrolled before you can enroll fields for the producer.
- - Includes the farm location (farm ID, state, county, field acres)
- - Other attributes about the farm (organic, CSAF, motivations, etc)
- **Make sure to include a correct Enrollment Year + Enrollment Quarter**. This will be used to make sure Producer Enrollment is reported in the correct quarterly submission.

![Producer Enrollment](https://gitlab.com/OpenTEAMAg/codesign/pcsc-farmos-module/-/raw/ac936ef3357e91172e5b4cd2b5e65373f6e85dec/docs/assets/producer-enrollment.png)

### Field Enrollment

- Field Enrollments are associated with a given Producer. The field enrollment contains information about the field that will be consistent for all practices and commodities in that field:
> - Contract start date
> -  Tract ID, Field ID, State, County
> - Field acres, Field land use, historical info
> - Field Enrollments also create a FarmOS Land Asset that stores the Field’s Geometry and make it visible on a map. See “FarmOS Land Asset” below for more information on this.

- Create new Field Enrollments by navigating to a Producer’s page and clicking “Field Enrollment” in the top-right or “Add a Field” in the table-list of Producer Fields. After creating, you click View + Edit to modify the Field Enrollment record. You can also click on the Land Asset link in this table.

![Field Enrollment](https://gitlab.com/OpenTEAMAg/codesign/pcsc-farmos-module/-/raw/ac936ef3357e91172e5b4cd2b5e65373f6e85dec/docs/assets/field-enrollment.png)

- **After a Field is enrolled you should**:
- - Add each Practice to be implemented on the Field (See Field Practices below)
- - - You can also add Field Practices in the same step as adding a Field Enrollment if the Practices are known at the time of field enrollment.
- - Add one or more Commodities that will be associated with the field (See Field Commodities below)

![Field Enrollment 2](https://gitlab.com/OpenTEAMAg/codesign/pcsc-farmos-module/-/raw/ac936ef3357e91172e5b4cd2b5e65373f6e85dec/docs/assets/field-enrollment2.png)

### Field Practices 

- Field Practices are associated with a single Field Enrolment for a producer.
- - Each Practice record has general info:
> - - Practice standard (most likely NRCS)
> - - Implementation year
> - - Extent + extent unit
- And questions specific to the NRCS Practice Type that are reported in the “Supplemental DPR workbook”:
> - Eg: Species category, Grazing Type, Nutrient type, Application method, etc…

- Create Field Practices by navigating to a Field Enrollment and clicking “Add a Practice” in the Practices table. After creating, you can click View + Edit to see the Practice record.

![Field Practices](https://gitlab.com/OpenTEAMAg/codesign/pcsc-farmos-module/-/raw/ac936ef3357e91172e5b4cd2b5e65373f6e85dec/docs/assets/practice.png)

### Field Commodities

- Field Commodities are associated with a single Field Enrollment for a producer. Each Commodity record has:
- - Commodity category + commodity type (please make sure these two “match up”, we currently do not have validation logic for this!)
- - Baseline yield + unit + location

- Create Field Commodities by navigating to a Field Enrollment and clicking “Add a Commodity” in the Commodities table. After creating, you can click View + Edit to see the Commodity record.

![Commodity Enrollment](https://gitlab.com/OpenTEAMAg/codesign/pcsc-farmos-module/-/raw/ac936ef3357e91172e5b4cd2b5e65373f6e85dec/docs/assets/commodity-enrollment.png)

### Farm Summary

- Farm Summaries are entered quarterly whenever there are updates or changes to the enrolled Farm. These records contain data about technical assistance and incentives received during the quarter:
- - TA types (up to 3)
- - Incentive reasons, structure and type (up to 3 each)
- - Incentive amount
- - Payments for enrollment, implementation, harvest, mmrv and sale (partial, full and none options)

> **Question:** Do we want to make any of these questions required for Farm Summaries? Will there always be at least one incentive reason, structure, type, etc? 

> We also determined that Farm Summary is only submitted to the DPR upon completion and verification of practice. What does this mean for when we collect and export this data?

- Create Farm Summaries by navigating to a Producer Page and clicking “Create Farm Summary” in the Farm Summaries table. After creating, you can click View + Edit to see the Farm Summary record.

![farm summary](https://gitlab.com/OpenTEAMAg/codesign/pcsc-farmos-module/-/raw/ac936ef3357e91172e5b4cd2b5e65373f6e85dec/docs/assets/farm-summary.png)

- You can also navigate to the “PCSC Farm Summaries” report from the menu to search Farm Summaries across all producers.

![farm summary](https://gitlab.com/OpenTEAMAg/codesign/pcsc-farmos-module/-/raw/ac936ef3357e91172e5b4cd2b5e65373f6e85dec/docs/assets/farm-summary2.png)

### Field Summary

- Field Summaries are entered quarterly/annually and are associated with a single **Field Commodity** for a given Field Enrollment. This is important! Field Summaries ask for various information:
- - Specific to a Commodity and Field + Practice:
- - - Commodity value, commodity volume + unit

- But also ask for other information that would be repeated for other commodities on the same Field. This information may need to be entered consistently across multiple Field Summaries:
- - Implementation cost, cost coverage
- - Official GHG model data

- Create Field Summaries by navigating to a Producer Page and clicking “Create Field Summary” in the Field Summaries table. After creating, you can click View + Edit to see the Field Summary record.

![Field Summary](https://gitlab.com/OpenTEAMAg/codesign/pcsc-farmos-module/-/raw/ac936ef3357e91172e5b4cd2b5e65373f6e85dec/docs/assets/field-summary.png)

> **Question:** DPR says "quarterly/annually as necessary". Does this differ by practice? Let’s figure out this workflow!

> Which of these questions need to be "Required"?


## FarmOS Land Asset

- All the important and valuable things on a farm are represented as "Assets" in farmOS. Asset types include Land, Plants, Animals, Equipment, Structures, etc. We use Land Assets in FarmOS to represent Field-level geometry associated with a Field Enrollment. Land assets are given a name in the form “Producer: Field ID”
- Over time, we may use these Land assets to store additional field-level data and associate with other types of management data (Log records)
- See Land Asset documentation [here](https://farmos.org/guide/assets/#land)

### Exporting records

- KML
- - Land assets (and Logs) can be exported to a KML file after they have been created with a geometry.
- - From the menu navigate to Records -> Assets -> Land. Select the records you want to include in your export, and select the "Export KML" option that appears at the bottom. A new KML file will be generated with all the geometries that were selected.
- CSV
- See Exporting Data docs [here](https://farmos.org/guide/export/)


### Importing KML
- Geospatial data
- - KML, KMZ, GPX and GeoJSON data can be imported to individual Asset and Log records by uploading the file to record and selecting “Import geometry from uploaded files” below the map editor. More detailed instructions here: https://farmos.org/guide/import/#geometry
- CSV
- - See also the general Importing Data docs [here](https://farmos.org/guide/import/)



