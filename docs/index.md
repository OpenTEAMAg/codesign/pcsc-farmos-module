# About

![Farmer & Cow](https://gitlab.com/OpenTEAMAg/codesign/pcsc-farmos-module/-/raw/7b2a453936690c0cd7b9f7f80e1bd70f55786ef4/docs/assets/moo.jpg)
*Photo by Vic Spindler-Fox*


## About
**This is a module built on top of farmOS in order to collect, store, and manage the data required for filling out the USDA PCSC reporting workbook for the Action for Climate-Smart Commodities project. This includes field-level and farm-level information that gets aggregated into project-level information about crops, practices, and greenhouse gasses.**  This includes field-level and farm-level information that gets aggregated into project-level information about crops, practices, and greenhouse gasses. 

> * [Partnerships for Climate-Smart Commodities Data Dictionary (PDF, 1.7 MB)](https://www.usda.gov/sites/default/files/documents/partnerships-climate-smart-commodities-data-dictionary.pdf)
> * [Partnerships for Climate-Smart Commodities Project Reporting Workbook (XLSX, 6.9 MB)](https://www.usda.gov/sites/default/files/documents/pcsc-project-reporting-workbook-spreadsheet.xlsx)
> * [Partnerships for Climate-Smart Commodities Supplemental Reporting Workbook (XLSX, 1.0 MB)](https://www.usda.gov/sites/default/files/documents/pcsc-supplemental-reporting-workbook.xlsx)

This is a collaborative project between [OpenTEAM](https://openteam.community/), [farmOS](https://farmos.org/), and partners in support of our USDA Partnership for Climate-Smart Commodities project, [Action for Climate-Smart Agriculture](https://www.wolfesneck.org/partnerships-for-climate-smart-commodities/).  

This work is supported by a subaward from a federal grant, led by [Wolfe’s Neck Center for Agriculture and the Environment](https://www.wolfesneck.org/).
This material is based upon work supported by the U.S. Department of Agriculture, under agreement number NR233A750004G032.

Relevant data and stories will be shared with other USDA project awardees and USDA leadership to inform the next generation of grant funding and other climate-smart infrastructure supports. Data will only be shared by the USDA in aggregate and de-identified. Learn more about USDA’s Partnerships for Climate-Smart Commodities [here](https://www.usda.gov/climate-solutions/climate-smart-commodities).


## Project Details

These categories are based on FAIR in Practice criteria developed in partnership between the [OpenTEAM Fair Tech Registry Collabathon](https://openteamag.gitlab.io/codesign/fair-registry/#what-do-we-mean-by-fair-and-fair-in-practice) and [Meridian](https://merid.org/) Conservation Benefits Catalog project as important for attestation.

| Category | Description   |
| -----------          | ----------- | 
| Name                 | farmOS PCSC Data Collection Module    | 
| Short name       | PCSC farmOS module | 
| Agency or Organization    | farmOS, OpenTEAM |         Both   | 
| Asset Type           | Software module    | 
| Model or software class      | Data collection, data storage                      |    CBC        |
| Link to asset, source site, documentation  | This module lives at https://action.farmos.net/ (permission required). In addition to this site, information is available at the module's [Gitlab repo](https://github.com/mstenta/farm_pcsc).  
| Text description      | This is a module built on top of farmOS in order to collect, store, and manage the data required for filling out the USDA PCSC reporting workbook for the Action for Climate-Smart Commodities project. This includes field-level and farm-level information that gets aggregated into project-level information about crops, practices, and greenhouse gasses.  |      Both      | 
| Point of contact, contact email  |  Mike Stenta, mike@farmier.com, Vic Spindler-Fox, vic@openteam.community.                     
| Date of last update, version number  | Currently in development    |   
| Metadata             | N/A currently                           |   Both         |
| Available to the public?    | No, this version is available to TAPs and partners on our PCSC project for testing only.        |  
| Has API?   | Potentially in the future |      Both - in FTR as 'nice to have' info     |
| Built on/built with      |  The farmOS server is built on top of [Drupal](https://www.drupal.org/), and we are building in alignment with [Common Farm Conventions](https://openteamag.gitlab.io/codesign/pcsc-farmos-module/projects/#common-farm-conventions) in development through multiple cooperative partnerships.   |  
| System requirements      | Forthcoming | 
| License   | [GPL-2.0 license](https://github.com/mstenta/farm_pcsc?tab=GPL-2.0-1-ov-file)        
| Statement on data storage, data privacy management  |  Forthcoming 
| Context for use, use documentation   |  We intend to use this site as a resource for this, as well as the [farmOS forum](https://farmos.discourse.group/) and feedback resources produced for our internal team. Let us know if you have recommendations or would like to contribute! |
| Dataset start and end date   |  We plan to use this module for data collection starting with the release of the updated PCSC Workbook in July of 2024, and continue for the duration of our PCSC projects (approx. 5 years) |
| Dataset geographic region   | Our farm projects cover the Northeast, Midatlantic, and West Coast of the continental US. As of this update, we are still in the process of identifying and onboarding these participants.  |